all: timertest

.PHONY: clean

LDFLAGS += -lpthread

timertest: timertest.cpp
	$(CXX) ${CXXFLAGS} $? -o $@ $(LDFLAGS)

clean:
	rm -f timertest
