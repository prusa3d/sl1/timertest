/*
 * timertest is a tool for testing linux timer reliability
 * Copyright 2019, Prusa Research s.r.o.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Prusa Research s.r.o.
 * Partyzánská 188/7a,
 * 17000 Praha 7,
 * Czech Republic
 *
 * info@prusa3d.com
 */

#include <iostream>
#include <vector>
#include <queue>
#include <string>
#include <thread>
#include <time.h>

static const int DEFAULT_ITERATIONS = 10000000;
static const int64_t NSEC = 1000 * 1000 * 1000;
static const int64_t FORWARD_JUMP_THRESHOLD_NS = 1 * NSEC;

bool ok = true;

static void timer_monothonicity_test(uint64_t iterations) {
	std::cout << "Running worker thread, " << iterations << " iterations" << std::endl;
	
	uint64_t i = 0;
	while(i < iterations && ok) {
		++i;
		struct timespec tp1, tp2;
		clock_gettime(CLOCK_MONOTONIC_RAW, &tp1);
		clock_gettime(CLOCK_MONOTONIC_RAW, &tp2);
		int64_t diff_ns = (tp2.tv_sec * NSEC + tp2.tv_nsec) - (tp1.tv_sec * NSEC + tp1.tv_nsec);

		if (diff_ns < 0 || diff_ns > FORWARD_JUMP_THRESHOLD_NS) {
			std::cout << "Negative timer diff: " << diff_ns << std::endl;
			std::cout << "Worker thread finished - ERROR DETECTED" << std::endl;
			ok = false;
			return;
		}
	}
	
	std::cout << "Worker thread finished without errors being found" << std::endl;
}

int main(int argc, char *argv[]) {
	std::vector<std::string> args(argv + 1, argv + argc);
	
	std::cout << "Timer test" << std::endl;
	
	uint64_t iterations = DEFAULT_ITERATIONS;
	int threads = 1;
		
	if(args.size() == 1) {
		iterations = std::stol(args[0]);
	} else {
		std::cout << "Defaulting to " << DEFAULT_ITERATIONS << " iterations" << std::endl;
	}
	
	if(std::thread::hardware_concurrency() > 0) {
		threads = std::thread::hardware_concurrency();
	}
	
	std::cout << "Will run " << threads << " threads each performing " << iterations << " iterations" << std::endl;
	
	std::queue<std::thread> tasks;
	
	for(int i = 0; i < threads; ++i) {
		tasks.push(std::thread(timer_monothonicity_test, iterations));
	}
	
	while(!tasks.empty()) {
		tasks.front().join();
		tasks.pop();
	}
	
	if(ok) {
		std::cout << "All threads completed their planed iterations without errors" << std::endl;
		return 0;
	} else {
		std::cout << "An error has been found. Terminating without running all planned iterations" << std::endl;
		return -1;
	}
}
